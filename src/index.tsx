import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import * as firebase from "firebase";
import store from './store';
import {Provider} from 'react-redux';


const firebaseConfig = {
    apiKey: "AIzaSyDJeRWaWhGwyKvHITQknOs7lbI8_lAgYUA",
    authDomain: "vreal-soft-firebase.firebaseapp.com",
    databaseURL: "https://vreal-soft-firebase-default-rtdb.firebaseio.com",
    projectId: "vreal-soft-firebase",
    storageBucket: "vreal-soft-firebase.appspot.com",
    messagingSenderId: "988391608364",
    appId: "1:988391608364:web:4e139d5fe9ac94723ac189"
};

firebase.default.initializeApp(firebaseConfig);


ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root'));


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
