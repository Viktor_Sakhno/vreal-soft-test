export type PathsDto = {
    id: number;
    title: string;
    shortDescription: string;
    fullDescription: string;
    length: string;
    favorite: boolean;
    markers: MarkerLocation[]
};

export type MarkerLocation = {
    id: number,
    location: {
        lat: number,
        lng: number
    },
}