import * as firebase from 'firebase';
import {AppDispatch} from './store';
import {PathsDto} from './dto/PathsDto';

export const getDataApi = (dispatch: AppDispatch, success: Function, failure: Function) => {
    const db = firebase.default.database();
    db.ref('data').on('value', (snapshot) => {
        const stateFromDB = snapshot.val();
        if (stateFromDB) {
            dispatch(success(stateFromDB));
        }else {
            dispatch(failure());
        }
    });
}

export const setDataApi = (data: PathsDto[], dispatch: AppDispatch, success: Function, failure: Function) => {
    const db = firebase.default.database();
    db.ref('data')
        .set(data)
        .then(dispatch(success(data)))
        .catch(dispatch(failure()));
}