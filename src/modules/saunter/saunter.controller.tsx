import React, {useState} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import {SaunterHeader} from './components/saunter-header/saunter-header';
import {useDispatch, useSelector} from 'react-redux';
import {SaunterSearchInput} from './components/saunter-search-input/saunter-search-input';
import {PathsDto} from '../../dto/PathsDto';
import s from './saunter.module.css';
import {SaunterList} from './components/saunter-list/saunter-list';
import {SaunterSingleItem} from './components/saunter-single-item/saunter-single-item';
import {CustomModal} from './components/custom-modal/custom-modal';
import {addFavoritePathApi, addPathApi, deleteFavoritePathApi, deletePathApi} from './services/actions';

export const SaunterController = () => {
    const [filteredData, setFilteredData] = useState<PathsDto[] | undefined>(undefined);
    const [singlePath, setSinglePath] = useState<PathsDto | undefined>(undefined);
    const [modalShow, setModalShow] = React.useState(false);

    const dispatch = useDispatch();

    const paths = useSelector(state => state.paths.paths);
    const filteredPaths = filteredData ? filteredData : paths;
    const sortedPaths = filteredPaths.slice().sort(path => path.favorite ? -1 : 1);

    const onHide = () => {
        setModalShow(prev => !prev)
    }

    const onSearch = (field: string) => {
        const filtered = paths.filter(({title, fullDescription}) => {
            return title.toLowerCase().includes(field.toLowerCase())
                || fullDescription.toLowerCase().includes(field.toLowerCase());
        });
        setFilteredData(filtered);
    }

    const addPath = (path:PathsDto) => {
        dispatch(addPathApi(paths.concat(path)));
    }

    const deletePath = (id:number) => {
        dispatch(deletePathApi(paths.filter(item => item.id !== id)));
    }

    const addFavoritePath = (path: PathsDto) => {
        const newPath = {...path, favorite: true};
        const newPaths = paths.filter(item => item.id !== path.id);
        dispatch(addFavoritePathApi(newPaths.concat(newPath)));
        setSinglePath(newPath);
    }

    const deleteFavoritePath = (path: PathsDto) => {
        const newPath = {...path, favorite: false};
        const newPaths = paths.filter(item => item.id !== path.id);
        dispatch(deleteFavoritePathApi(newPaths.concat(newPath)));
        setSinglePath(newPath);
    }

    return (
        <>
            <Container fluid="xl" className="p-4">
                <SaunterHeader onHide={onHide}/>

                <Row className="pt-4 d-block  d-md-flex">
                    <Col className={`border-right border-dark ${s.searchList}`}>
                        <SaunterSearchInput onSearch={onSearch}/>
                        <SaunterList data={sortedPaths} setSinglePath={setSinglePath}/>
                    </Col>
                    <Col>
                        <SaunterSingleItem
                            path={singlePath}
                            deletePath={deletePath}
                            setSinglePath={setSinglePath}
                            addFavoritePath={addFavoritePath}
                            deleteFavoritePath={deleteFavoritePath}
                        />
                    </Col>
                </Row>

            </Container>
            <CustomModal
                show={modalShow}
                onHide={onHide}
                addPath={addPath}
            />
        </>
    );
}