import React, {ChangeEvent, FC} from 'react';
import {FormControl, InputGroup} from 'react-bootstrap';
import { ImSearch } from "react-icons/im";


type Props = {
    onSearch: (field: string) => void;
};
export const SaunterSearchInput: FC<Props> = ({onSearch}) => {
    const handler = (e: ChangeEvent<HTMLInputElement>) => {
        onSearch(e.target.value);
    }

    return (
        <InputGroup className="mb-3">
            <FormControl
                placeholder="Search..."
                aria-label="Search..."
                aria-describedby="basic-addon1"
                onChange={handler}
                className="border-dark border-right-0"
            />
            <InputGroup.Append>
                <InputGroup.Text id="basic-addon2" className="border-dark border-left-0 bg-white"><ImSearch/></InputGroup.Text>
            </InputGroup.Append>
        </InputGroup>
    );
}
