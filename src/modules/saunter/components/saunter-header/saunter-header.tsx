import React, {FC} from 'react';
import {Button} from 'react-bootstrap';
import {HiArrowsExpand} from "react-icons/hi";

type Props = {
    onHide: () => void;
};
export const SaunterHeader: FC<Props> = ({onHide}) => {
    return (
        <div className="d-flex justify-content-between pb-4 flex-wrap border-bottom border-dark">
            <div className="d-flex align-items-center">
                <HiArrowsExpand size={35} className="mr-3"/>
                <h2 className="mb-0 mr-2">Saunter</h2>
            </div>
            <Button onClick={onHide} variant="primary" className="font-weight-bold">Add path</Button>
        </div>
    );
}
