import React, {FC, useEffect, useState} from 'react';
import {GoogleMap, Marker, Polyline} from '@react-google-maps/api';
import {MarkerLocation} from '../../../../dto/PathsDto';
import s from './google-map.module.css'
import {Button} from 'react-bootstrap';
import {FaMapMarkerAlt} from 'react-icons/fa';

type Coordinate = {
    lat: number,
    lng: number
}

type Props = {
    setLength: (length: string) => void,
    markers: MarkerLocation[] | undefined;
    setMarkers: (markers: MarkerLocation[]) => void;

};


export const GoogleMapContainer: FC<Props> = ({setLength, markers, setMarkers}) => {
    const [currentPosition, setCurrentPosition] = useState<Coordinate>();
    const [pathsCoordinate, setPathsCoordinate] = useState<Coordinate[]>([]);

    useEffect(() => {
        getPathCoordinate();
        getLength();
    }, [markers])

    const onMarkerDragEnd = (e: google.maps.MapMouseEvent, id: number) => {
        const index = markers?.findIndex(item => item.id === id);
        const lat = e.latLng.lat();
        const lng = e.latLng.lng();
        const marker = {id, location: {lat, lng}}
        const newMarkers = markers?.slice();
        newMarkers?.splice(index as number, 1, marker);
        setMarkers(newMarkers as MarkerLocation[]);
    };

    const addMarker = (e: google.maps.MapMouseEvent) => {
        const id = +new Date();
        const lat = e.latLng.lat();
        const lng = e.latLng.lng();
        const marker = {id, location: {lat, lng}}
        setMarkers(markers ? markers.concat(marker) : [marker]);
        setCurrentPosition(marker.location);
    }

    const addDefaultMarker = () => {
        if(markers) {
            const id = +new Date();
            const lat = markers[markers.length -1].location.lat - 0.0005;
            const lng = markers[markers.length -1].location.lng - 0.0005;
            const marker = {id, location: {lat, lng}};
            setMarkers(markers ? markers.concat(marker) : [marker]);
            setCurrentPosition(marker.location);
        }else {
            const defaultLocation = currentPosition ? currentPosition : defaultCenter
            const id = +new Date();
            const marker = {id, location: {lat: defaultLocation.lat, lng: defaultLocation.lng}}
            setMarkers( [marker]);
            setCurrentPosition(marker.location);
        }

    }

    const deleteMarker = (id: number) => {
        const index = markers?.findIndex(item => item.id === id);
        const newMarkers = markers?.slice();
        newMarkers?.splice(index as number, 1);
        setMarkers(newMarkers as MarkerLocation[]);
        setLength('0');
    }

    const getPathCoordinate = () => {
        if (markers) {
            setPathsCoordinate(markers.reduce((acc, item) => acc.concat(item.location), [] as Coordinate[]));
        }
    }

    const getLength = () => {
        if (markers && markers.length > 1) {
            const arrayLength: number[] = [];
            markers?.reduce((prev, next) => {
                const a = new google.maps.LatLng(prev.location.lat, prev.location.lng);
                const b = new google.maps.LatLng(next.location.lat, next.location.lng);
                arrayLength.push(google.maps.geometry.spherical.computeDistanceBetween(a, b));
                return next;
            })
            const result = arrayLength.reduce((acc, next) => acc + next, 0);
            let length;
            if(result / 1000 > 1){
                length = (result / 1000).toFixed(2) + 'km'
            }else {
                length = (result / 1000).toFixed(3).slice(2) + 'm';
            }
            setLength(length);
        }
    }

    useEffect(() => {
        navigator.geolocation.getCurrentPosition(success, error, {timeout: 5000});
    }, [])
    const success = (position: GeolocationPosition) => {
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;
        const currentPosition = {
            lat: latitude,
            lng: longitude
        }
        setCurrentPosition(currentPosition);
    }
    const error = (err: GeolocationPositionError) => {
        console.warn(`ERROR(${err.code}): ${err.message}`);
    };
    const defaultCenter = {
        lat: 49.43961118688491,
        lng: 32.06255874175739
    }

    const mapStyles = {
        height: "100%",
        width: "100%"
    };

    const options = {
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        clickable: false,
        draggable: false,
        editable: false,
        visible: true,
        radius: 30000,
        zIndex: 1
    };

    return (
        <div style={{position: 'relative', width: "100%", height: '100%'}}>
            <GoogleMap
                mapContainerStyle={mapStyles}
                zoom={13}
                center={currentPosition ? currentPosition : defaultCenter}
                onClick={addMarker}
            >
                {
                    markers?.map(item => {
                        return (
                            <Marker
                                key={item.id}
                                position={item.location}
                                draggable={true}
                                onDragEnd={(e) => onMarkerDragEnd(e, item.id)}
                                onClick={() => deleteMarker(item.id)}
                            />
                        )
                    })
                }
                <Polyline
                    path={pathsCoordinate}
                    options={options}
                />
            </GoogleMap>
            <div className={s.markerBtn}>
                <Button onClick={addDefaultMarker}  variant="light" className="d-flex align-items-center">
                    <FaMapMarkerAlt className="mr-2"/>
                    Add marker
                </Button>
            </div>
        </div>
    )
}