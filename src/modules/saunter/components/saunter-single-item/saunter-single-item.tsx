import React, {FC} from 'react';
import {PathsDto} from '../../../../dto/PathsDto';
import s from './saunter-single-item.module.css'
import {GoogleMap, Marker, Polyline} from '@react-google-maps/api';

type Props = {
    path: PathsDto | undefined,
    deletePath: (id: number) => void,
    setSinglePath: (path: undefined) => void,
    addFavoritePath: (path: PathsDto) => void,
    deleteFavoritePath: (path: PathsDto) => void,
};

type Coordinate = {
    lat: number,
    lng: number
}

export const SaunterSingleItem: FC<Props> = ({path, deletePath, setSinglePath, addFavoritePath, deleteFavoritePath}) => {
    if (path === undefined) {
        return null;
    }

    const handler = () => {
        deletePath(path.id);
        setSinglePath(undefined);
    }

    const pathsCoordinate = path.markers.reduce((acc, item) => acc.concat(item.location), [] as Coordinate[])

    const mapStyles = {
        width: '100%',
        height: '40vh'
    };

    const options = {
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        clickable: false,
        draggable: false,
        editable: false,
        visible: true,
        radius: 30000,
        zIndex: 1
    };

    return (
        <div className={s.singleItem}>
            <div className="d-flex justify-content-between align-items-center">
                <div className="h4 mb-0">
                    {path.title}
                </div>
                <div className="h5 mb-0">
                    {path.length}
                </div>
            </div>
            <div className="mt-3 mb-4">
                {path.fullDescription}
            </div>
            <GoogleMap
                mapContainerStyle={mapStyles}
                zoom={13}
                center={pathsCoordinate[0]}
            >
                {
                    path.markers.map(item => {
                        return (
                            <Marker
                                key={item.id}
                                position={item.location}
                            />
                        )
                    })
                }
                <Polyline
                    path={pathsCoordinate}
                    options={options}
                />
            </GoogleMap>
            <div className={s.favorite}>
                {path.favorite ?
                    <span className={s.favorite__btn} onClick={() => deleteFavoritePath(path)}>
                        <span >Remove</span>&nbsp;
                        <span >from</span>&nbsp;
                        <span >favorite</span>
                    </span>
                    :
                    <span className={s.favorite__btn} onClick={() => addFavoritePath(path)}>
                        <span >Add</span>&nbsp;
                        <span >to</span>&nbsp;
                        <span >favorite</span>
                    </span>
                }
            </div>
            <div className="text-right">
                <span onClick={handler} className={s.delete}>
                    Remove
                </span>
            </div>
        </div>
    );
}
