import React, { FC } from 'react';
import {PathsDto} from '../../../../dto/PathsDto';
import s from './saunter-list-item.module.css'
import {HiArrowsExpand} from "react-icons/hi";
import { AiFillStar } from "react-icons/ai";
import {MdKeyboardArrowRight} from 'react-icons/md';

type Props = {
    path: PathsDto;
    setSinglePath: (path: PathsDto) => void;
};
export const SaunterListItem: FC<Props> = ({path, setSinglePath}) =>  {
    const handler = () => {
        setSinglePath(path)
    }
  return (
      <div className={`d-flex justify-content-between align-items-center bg-light mb-2 p-2 ${s.item}`} key={path.id} onClick={handler}>
          <div><HiArrowsExpand style={{minWidth: '20px'}} size={35} className="ml-2"/></div>
          <div className="flex-grow-1 ml-2 mr-2">
              <div className="d-flex align-items-center">
                  {path.favorite
                      ? <AiFillStar fill="#007bff" className="mr-1"/>
                      : null}
                  <div className="h6 mb-0">{path.title}</div>
              </div>
              <div className={s.shortDescription}><small>{path.shortDescription}</small></div>
          </div>
          <div className="h5 mb-0 mr-2">
              {path.length}
          </div>
          <div>
              <MdKeyboardArrowRight size={25}/>
          </div>
      </div>
  );
}
