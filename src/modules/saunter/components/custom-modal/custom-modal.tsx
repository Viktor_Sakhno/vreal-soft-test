import React, {FC, useState} from 'react';
import {Button, Col, Form, Modal, Row} from 'react-bootstrap';
import s from './custom-modal.module.css'
import {MarkerLocation, PathsDto} from '../../../../dto/PathsDto';
import {GoogleMapContainer} from '../google-map/google-map';
import {GrMapLocation} from 'react-icons/gr';
import {TiTick} from 'react-icons/ti';

type Props = {
    show: boolean;
    onHide: () => void;
    addPath: (path: PathsDto) => void;
};

export const CustomModal: FC<Props> = ({show, onHide, addPath}) => {
    const [title, setTitle] = useState<string>('');
    const [shortDescription, setShortDescription] = useState<string>('');
    const [fullDescription, setFullDescription] = useState<string>('');

    const [length, setLength] = useState<string>('0');
    const [markers, setMarkers] = useState<MarkerLocation[]>();

    const setEmpty = () => {
        setTitle('');
        setShortDescription('');
        setFullDescription('');
    }

    const handleClose = () => {
        onHide();
        setEmpty();
        setLength('0');
        setMarkers(undefined);
    }

    const handler = () => {
        const data = markers as MarkerLocation[];
        const path = {id: +new Date(), title, shortDescription, fullDescription, length, favorite: false, markers: data}
        handleClose();
        addPath(path);
        setLength('0');
        setMarkers(undefined);
    }

    return (
        <Modal
            show={show}
            onHide={handleClose}
            size="xl"
            dialogClassName={s.customModal}
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Add new path
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Row className={s.modalRow}>
                    <Col className={s.modalColForm}>
                        <Form>
                            <Form.Group controlId="title">
                                <Form.Label>Title</Form.Label>
                                <Form.Control onChange={event => setTitle(event.target.value)} type="text"
                                              placeholder="Text input"/>
                            </Form.Group>
                            <Form.Group controlId="shortDescription">
                                <Form.Label>Short Description</Form.Label>
                                <Form.Control onChange={event => setShortDescription(event.target.value)} as="textarea"
                                              rows={3} placeholder="Text area"/>
                                <Form.Text className={`float-right ${shortDescription.length > 160 ? 'text-danger' : 'text-muted'}`}>
                                    {`Limit ${shortDescription.length} of 160`}
                                </Form.Text>
                            </Form.Group>
                            <Form.Group controlId="fullDescription">
                                <Form.Label>Full Description</Form.Label>
                                <Form.Control onChange={event => setFullDescription(event.target.value)} as="textarea"
                                              rows={5} placeholder="Text area"/>
                            </Form.Group>
                        </Form>
                        <div className={s.length}>
                            <GrMapLocation className="mr-2" size={20}/>Length {length}
                        </div>
                        <div className="d-flex justify-content-center">
                            <Button className="border d-flex align-items-center"
                                    disabled={!(title && shortDescription && fullDescription
                                        && (markers ? markers.length > 1 : markers) && shortDescription.length <= 160)}
                                    onClick={handler} variant="light">
                                <TiTick/>
                                Add path
                            </Button>
                        </div>
                    </Col>
                    <Col className={s.modalColMap}>
                        <GoogleMapContainer setLength={setLength} setMarkers={setMarkers} markers={markers}/>
                    </Col>
                </Row>
            </Modal.Body>

        </Modal>
    );
}