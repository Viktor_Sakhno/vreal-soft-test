import React, { FC } from 'react';
import {PathsDto} from '../../../../dto/PathsDto';
import {SaunterListItem} from '../saunter-list-item/saunter-list-item';
import s from './saunter-list.module.css'



type Props = {
    data: PathsDto[];
    setSinglePath: (path: PathsDto) => void;
};
export const SaunterList: FC<Props> = ({data, setSinglePath}) =>  {
  return (
      <div className={s.list}>
          {data.map((path) => (
              <SaunterListItem key={path.id} path={path} setSinglePath={setSinglePath}/>
          ))}

      </div>
  );
}
