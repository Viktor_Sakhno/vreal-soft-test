import {AppDispatch} from '../../../store';
import {
    addFavoritePathFailure,
    addFavoritePathSuccess,
    addPathFailure,
    addPathSuccess,
    deleteFavoritePathFailure,
    deleteFavoritePathSuccess,
    deletePathFailure,
    deletePathSuccess,
    getPathsFailure,
    getPathsSuccess
} from './index';
import {PathsDto} from '../../../dto/PathsDto';
import {getDataApi, setDataApi} from '../../../api';

export const getPathsApi = () => (dispatch: AppDispatch) => {
    getDataApi(dispatch, getPathsSuccess, getPathsFailure);
};

export const addPathApi = (data: PathsDto[]) => (dispatch: AppDispatch) => {
    setDataApi(data, dispatch, addPathSuccess, addPathFailure);
};

export const deletePathApi = (data: PathsDto[]) => (dispatch: AppDispatch) => {
    setDataApi(data, dispatch, deletePathSuccess, deletePathFailure);
};

export const addFavoritePathApi = (data: PathsDto[]) => (dispatch: AppDispatch) => {
    setDataApi(data, dispatch, addFavoritePathSuccess, addFavoritePathFailure);
};

export const deleteFavoritePathApi = (data: PathsDto[]) => (dispatch: AppDispatch) => {
    setDataApi(data, dispatch, deleteFavoritePathSuccess, deleteFavoritePathFailure);
};