import reducer, {
    getPathsRequest,
    getPathsSuccess,
    getPathsFailure,
    addPathRequest,
    addPathSuccess,
    addPathFailure,
    deletePathRequest,
    deletePathSuccess,
    deletePathFailure,
    addFavoritePathRequest,
    addFavoritePathSuccess,
    addFavoritePathFailure,
    deleteFavoritePathRequest,
    deleteFavoritePathSuccess,
    deleteFavoritePathFailure
} from './saunter-slice';

export default reducer;

export {
    getPathsRequest,
    getPathsSuccess,
    getPathsFailure,
    addPathRequest,
    addPathSuccess,
    addPathFailure,
    deletePathRequest,
    deletePathSuccess,
    deletePathFailure,
    addFavoritePathRequest,
    addFavoritePathSuccess,
    addFavoritePathFailure,
    deleteFavoritePathRequest,
    deleteFavoritePathSuccess,
    deleteFavoritePathFailure
}
