import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {PathsDto} from '../../../dto/PathsDto';

export type INIT_STATE = {
    paths: PathsDto[]
};

const initialState: INIT_STATE = {
    paths: []
}

const testStoreSlice = createSlice({
    name: 'paths',
    initialState: initialState,
    reducers: {
        getPathsRequest: state => {
            return state;
        },
        getPathsSuccess: (state, action: PayloadAction<PathsDto[]>) => {
            state.paths = action.payload;
            return state;
        },
        getPathsFailure: state => {
            return state;
        },
        addPathRequest: state => {
            return state;
        },
        // addPathSuccess: (state, action: PayloadAction<PathsDto>) => {
        //     state.paths = state.paths.concat(action.payload);
        //     return state;
        // },
        addPathSuccess: (state, action: PayloadAction<PathsDto[]>) => {
            state.paths = action.payload;
            return state;
        },
        addPathFailure: state => {
            return state;
        },
        deletePathRequest: state => {
            return state;
        },
        // deletePathSuccess: (state, action: PayloadAction<number>) => {
        //     state.paths = state.paths.filter(item => item.id !== action.payload);
        //     return state;
        // },
        deletePathSuccess: (state, action: PayloadAction<PathsDto[]>) => {
            state.paths = action.payload;
            return state;
        },
        deletePathFailure: state => {
            return state;
        },
        addFavoritePathRequest: state => {
            return state;
        },
        addFavoritePathSuccess: (state, action: PayloadAction<PathsDto[]>) => {
            state.paths = action.payload;
            return state;
        },
        addFavoritePathFailure: state => {
            return state;
        },
        deleteFavoritePathRequest: state => {
            return state;
        },
        deleteFavoritePathSuccess: (state, action: PayloadAction<PathsDto[]>) => {
            state.paths = action.payload;
            return state;
        },
        deleteFavoritePathFailure: state => {
            return state;
        }
    }
});

const {actions, reducer} = testStoreSlice;

export const {
    getPathsRequest,
    getPathsSuccess,
    getPathsFailure,
    addPathRequest,
    addPathSuccess,
    addPathFailure,
    deletePathRequest,
    deletePathSuccess,
    deletePathFailure,
    addFavoritePathRequest,
    addFavoritePathSuccess,
    addFavoritePathFailure,
    deleteFavoritePathRequest,
    deleteFavoritePathSuccess,
    deleteFavoritePathFailure
} = actions;

export default reducer;