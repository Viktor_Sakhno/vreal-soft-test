import {combineReducers} from "@reduxjs/toolkit"
import paths from '../modules/saunter/services'

const rootReducer = combineReducers({
    paths
});

export default rootReducer;

export type AppState = ReturnType<typeof rootReducer>;

declare module 'react-redux' {
    export interface DefaultRootState extends AppState {
    }
}

