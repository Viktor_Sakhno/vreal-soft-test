import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {SaunterController} from './modules/saunter/saunter.controller';
import {getPathsApi} from './modules/saunter/services/actions';
import {LoadScript} from '@react-google-maps/api';


function App() {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getPathsApi());
    }, [])

    return (
        <LoadScript
            googleMapsApiKey='AIzaSyBnEp_gg8VFHPbd2QnFYX7VM5dcNFRzEsA'>
            <SaunterController/>
        </LoadScript>

    );
}

export default App;
